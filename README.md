# THIS PIECE OF SOFTWARE IS DEPRECATED

Use [foot] instead.

[foot]: https://codeberg.org/dnkl/foot/

---

[khansoul](https://pypi.org/project/khansoul/) is a simple VTE-based
terminal emulator written in Python.

## Dependencies

External:

* Python 3
* GTK 3
* VTE
* make - build-time
* scdoc - build-time for the man pages

PyPI:

* PyGObject
* xdg

## How to install

```
pip3 install [--user] khansoul
```

## How to configure

See the `khansoul(5)` man page.
