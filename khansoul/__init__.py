__title__ = "khansoul"
__description__ = "Simple terminal emulator"
__version__ = "2.1.0"

def main():
	"""Launches the terminal normally."""
	
	import os, xdg
	
	args = parse_args(os.sys.argv[1:])
	
	if args.config != None:
		config = parse_config(
			read_config_file(args.config, True)
		)
	else:
		config = parse_config(
			read_config_file(
				xdg.XDG_CONFIG_HOME / f"{__title__}/config",
			False)
		)
	
	setup_terminal(args, config)

def die(*args, **kwargs):
	"""Prints the error message and closes the program."""
	
	import os
	
	print(f"{os.sys.argv[0]}: error:", *args, **kwargs, file=os.sys.stderr)
	
	os.sys.exit(1)

def parse_args(args):
	"""
	Parses the command line arguments.
	
	# Arguments
	
	* `args` - the array of the command line arguments
	
	# Returns
	
	The namespace of the parameters.
	"""
	
	import argparse
	
	arg_parser = argparse.ArgumentParser(
		description=__description__,
		epilog=f"See {__title__}(1) and {__title__}(5) for more info."
	)
	
	arg_parser.add_argument("-v", "--version",
		action="version",
		
		version=__title__ + " " + __version__
	)
	
	arg_parser.add_argument("-c", "--config",
		metavar="FILEPATH",
		
		help="custom config filepath"
	)
	
	arg_parser.add_argument("-e", "--execute",
		metavar="COMMAND",
		
		help="execute command"
	)
	
	return arg_parser.parse_args(args)

def parse_config(config):
	"""
	Parses the configuration file contents.
	
	# Arguments
	
	* `config` - the array of the unparsed parameters
	
	# Returns
	
	The namespace of the parameters.
	"""
	
	import os, argparse
	
	arg_parser = argparse.ArgumentParser(
		prog=f"{os.sys.argv[0]} (config file)",
		prefix_chars=".",
		add_help=False
	)
	
	window_states = ("normal", "minimized", "maximized", "fullscreen")
	
	text_blink_modes = ("never", "focused", "unfocused", "always")
	def TextBlinkMode(name):
		return text_blink_modes.index(name)
	
	cursor_blink_modes = ("system", "always", "never")
	def CursorBlinkMode(name):
		return cursor_blink_modes.index(name)
	
	cursor_shapes = ("block", "ibeam", "underline")
	def CursorShape(name):
		return cursor_shapes.index(name)
	
	arg_parser.add_argument(".no-title-bar",
		action="store_false"
	)
	
	arg_parser.add_argument(".window-state",
		choices=window_states
	)
	
	arg_parser.add_argument(".window-width",
		type=int
	)
	arg_parser.add_argument(".window-height",
		type=int
	)
	
	arg_parser.add_argument(".no-bold",
		action="store_false",
		dest="allow_bold"
	)
	
	arg_parser.add_argument(".no-bright-bold",
		action="store_false",
		dest="bold_is_bright"
	)
	
	arg_parser.add_argument(".no-hyperlinks",
		action="store_false",
		dest="allow_hyperlink"
	)
	
	arg_parser.add_argument(".no-audible-bell",
		action="store_false",
		dest="audible_bell"
	)
	
	arg_parser.add_argument(".background-color")
	arg_parser.add_argument(".foreground-color")
	
	arg_parser.add_argument(".palette")
	
	arg_parser.add_argument(".text-blink-mode",
		choices=text_blink_modes,
		
		type=TextBlinkMode
	)
	
	arg_parser.add_argument(".cursor-background-color")
	arg_parser.add_argument(".cursor-foreground-color")
	
	arg_parser.add_argument(".cursor-blink-mode",
		choices=cursor_blink_modes,
		
		type=CursorBlinkMode
	)
	
	arg_parser.add_argument(".cursor-shape",
		choices=cursor_shapes,
		
		type=CursorShape
	)
	
	arg_parser.add_argument(".font")
	
	arg_parser.add_argument(".no-scroll-on-keystroke",
		action="store_false",
		dest="scroll_on_keystroke"
	)
	
	arg_parser.add_argument(".no-scroll-on-output",
		action="store_false",
		dest="scroll_on_output"
	)
	
	arg_parser.add_argument(".scrollback-lines",
		type=int
	)
	
	return arg_parser.parse_args(config)

def read_config_file(path, is_custom):
	"""
	Reads the config file and splits it into an array of arguments.
	
	# Arguments
	
	* `path` - the filepath of the config file
	* `is_custom` - whether or not the filepath is provided through the
	  command line arguments
	
	# Returns
	
	The array of the arguments.
	"""
	
	import os, shlex
	
	args = []
	
	try:
		with open(path, "r") as config_file:
			args = shlex.split(config_file.read(), comments=True)
	except FileNotFoundError:
		if is_custom:
			die(f"{path}: file not found")
	
	return args

def setup_terminal(args, config):
	"""
	Opens the terminal window.
	
	# Arguments
	
	* `args` - the namespace of the parameters
	* `config` - the namespace of the style parameters
	"""
	
	import os, gi
	
	gi.require_version("GLib", "2.0")
	gi.require_version("Gdk", "3.0")
	gi.require_version("Gtk", "3.0")
	gi.require_version("Pango", "1.0")
	gi.require_version("Vte", "2.91")
	
	from gi.repository import GLib, Gdk, Gtk, Pango, Vte
	
	window = Gtk.Window()
	header_bar = Gtk.HeaderBar()
	terminal = Vte.Terminal()
	
	def update_window_title(_):
		window.set_title(terminal.get_window_title())
	
	def parse_color(color):
		if not color:
			return None
		
		rgba = Gdk.RGBA()
		
		if not rgba.parse(color):
			die(f"'{color}' is not a valid color")
		
		return rgba
	def parse_palette(raw_palette):
		if not raw_palette:
			return None
		
		palette = raw_palette.split(":")
		
		if not len(palette) in (8, 16, 256):
			die(f"{len(palette)} is not a valid palette size")
		
		rgba_list = []
		
		for color in palette:
			if not color:
				die("all of palette colors must be specified")
			
			rgba_list.append(parse_color(color))
		
		return rgba_list
	
	window.set_titlebar(header_bar)
	window.add(terminal)
	
	if config.window_width != None:
		window.props.default_width = config.window_width
	if config.window_height != None:
		window.props.default_height = config.window_height
	
	for property in (
		"allow_bold",
		"bold_is_bright",
		"allow_hyperlink",
		"audible_bell",
		"text_blink_mode",
		"cursor_blink_mode",
		"cursor_shape",
		"scroll_on_keystroke",
		"scroll_on_output",
		"scrollback_lines"
	):
		if vars(config)[property] != None:
			terminal.set_property(property, vars(config)[property])
	
	terminal.set_colors(
		parse_color(config.foreground_color),
		parse_color(config.background_color),
		parse_palette(config.palette)
	)
	terminal.set_color_cursor(parse_color(
		config.cursor_background_color
	))
	terminal.set_color_cursor_foreground(parse_color(
		config.cursor_foreground_color
	))
	terminal.set_font(Pango.FontDescription.from_string(
		config.font
	) if config.font else None)
	
	window.connect("destroy", Gtk.main_quit)
	
	terminal.connect("child-exited", Gtk.main_quit)
	terminal.connect("window-title-changed", update_window_title)
	
	child_program = [os.getenv("SHELL", "/bin/sh")]
	
	if args.execute != None:
		child_program.extend(["-c", args.execute])
	
	terminal.spawn_async(
		Vte.PtyFlags.DEFAULT,
		None, # Keep the initial child cwd the same as the parent cwd
		child_program,
		None, # Don't do anything with the environment variables
		GLib.SpawnFlags.DO_NOT_REAP_CHILD,
		None, # Don't do any child setup
		None, # See the comment above
		-1 # Let the shell take as much time as it wants to launch
	)
	
	window.show_all()
	
	if not config.no_title_bar:
		header_bar.hide()
	
	if config.window_state == "minimized":
		window.iconify()
	elif config.window_state == "maximized":
		window.maximize()
	elif config.window_state == "fullscreen":
		window.fullscreen()
	
	Gtk.main()
