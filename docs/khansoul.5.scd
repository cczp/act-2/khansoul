khansoul(5)

# NAME

khansoul - configuration file

# DESCRIPTION

By default, _$XDG\_CONFIG\_HOME/khansoul/config_ (_~/.config_ if
_$XDG\_CONFIG\_HOME_ isn't set) is checked. This file accepts
parameters in form of _.parameter value_ or _.parameter_ if the
parameter takes no value. Empty lines and lines that start with \# are
ignored.

# PARAMETERS

*.no-title-bar*
	Disable the title bar visibility.

*.window-state {normal,minimized,maximized,fullscreen}*
	Set the chosen window state (normal by default).

*.window-width WINDOW_WIDTH*
*.window-height WINDOW_HEIGHT*
	Set the window geometry.

*.no-bold*
	Disable displaying bold text as actually bold (enabled by
	default).

*.no-bright-bold*
	Disable bold text brightening.

*.no-hyperlinks*
	Disable hyperlink highlighting.

*.no-audible-bell*
	Disable the audible bell.

*.background-color BACKGROUND_COLOR*
	Set the background color.

*.foreground-color FOREGROUND_COLOR*
	Set the foreground color.

*.palette PALETTE*
	Set the palette (8, 16, or 256 colors separated by colons).

*.text-blink-mode {never,focused,unfocused,always}*
	Set the text blink mode (always by default).

*.cursor-background-color CURSOR_BACKGROUND_COLOR*
	Set the cursor background color.

*.cursor-foreground-color CURSOR_FOREGROUND_COLOR*
	Set the cursor foreground color.

*.cursor-blink-mode {system,always,never}*
	Set the cursor blink mode (system by default).

*.cursor-shape {block,ibeam,underline}*
	Set the cursor shape (block by default).

*.font FONT*
	Set the font.

*.no-scroll-on-keystroke*
	Disable scrolling the text when a key is pressed (enabled
	by default).

*.no-scroll-on-output*
	Disable scrolling when new output is recieved (enabled by
	default).

*.scrollback-lines SCROLLBACK_LINES*
	Set the number of scrollback lines (negative values result
	infinite scrollback).

# AUTHORS

Kirby Kevinson <kirbysemailaddress@protonmail.com>

# SEE ALSO

*khansoul*(1)
